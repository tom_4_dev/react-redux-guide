import { combineReducers } from 'redux';
import counter from './CounterReducer';
import result from './ResultReducer';

const rootReducer = combineReducers({
  ctr: counter,
  res: result,
});

export default rootReducer;
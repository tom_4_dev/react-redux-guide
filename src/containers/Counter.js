import React, { Component } from 'react';
import CounterSection from './../components/Counter';
import AddCounter from './../components/AddCounter';
import ResultLi from './../components/Result';
import { connect } from 'react-redux';
import * as actionCreators from './../store/actions/index';

class Counter extends Component {


  render() {
    return (
      <div>
        <CounterSection counter={this.props.ctr} />
        <AddCounter counterHandler={this.props.onIncCounter} label="Add" />
        <AddCounter counterHandler={this.props.onDecCounter} label="Delete" />
        <AddCounter counterHandler={this.props.onIncTenCounter} label="Add by 10" />

        <hr />
        <button onClick={() => this.props.onStoreResult(this.props.ctr)}>Store result</button>
        <ul>
          {this.props.storedResult.map(result => {
            return <ResultLi key={result.id} value={result.value} />
          })}
        </ul>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    ctr: state.ctr.counter,
    storedResult: state.res.results
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    onIncCounter: () => {
      dispatch(actionCreators.increment())
    },
    onDecCounter: () => {
      dispatch(actionCreators.decrement())
    },
    onIncTenCounter: () => {
      dispatch(actionCreators.incrementByTen(10))
    },
    onStoreResult: (result) => {
      dispatch(actionCreators.storeResult(result))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Counter);

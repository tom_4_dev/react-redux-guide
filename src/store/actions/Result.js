import * as actionTypes from './ActionConstant';

export const saveResult = (res) => {
  return {
    type: actionTypes.STORERESULT,
    value: res,
  }
}

export const storeResult = (val) => {
  return dispatch => {
    setTimeout(() => {
      dispatch(saveResult(val))
    }, 2000);
  }  
}

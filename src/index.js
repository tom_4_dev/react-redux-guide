import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { createStore, applyMiddleware, compose } from 'redux';
import rootReducer from './store/reducers/index';
import { Provider } from 'react-redux';
import thunk from'redux-thunk';
import registerServiceWorker from './registerServiceWorker';

const logger = (store) => {
  return next => {
    return action => {
      console.log("dispatching", action);
      const result = next(action);
      console.log("next state", store.getState());
      return result;
    }
  }
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(rootReducer, composeEnhancers(applyMiddleware(logger, thunk)));

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));
registerServiceWorker();

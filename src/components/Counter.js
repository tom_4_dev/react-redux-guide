import React from 'react';

const counter = (props) => {
  return (
    <div>
      Counter: {props.counter}
    </div>
  )
}

export default counter;
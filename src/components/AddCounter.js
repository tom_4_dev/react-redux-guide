import React from 'react';

const addCounter = (props) => {
  return (
    <div>
      <button onClick={props.counterHandler}>{props.label}</button>
    </div>
  )
}

export default addCounter;
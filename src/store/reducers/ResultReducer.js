import * as actionTypes from './../actions/ActionConstant';
import { updateObject } from './../utility';

const initialState = {
  results: []
}

const result = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.STORERESULT:
      return updateObject(state, { results: state.results.concat({ id: new Date(), value: action.value }) })
    default:
      return state;
  }
}

export default result;
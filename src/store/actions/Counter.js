import * as actionTypes from './ActionConstant';

export const increment = () => {
  return {
    type: actionTypes.INC
  }
}

export const decrement = () => {
  return {
    type: actionTypes.DEC
  }
}

export const incrementByTen = (val) => {
  return {
    type: actionTypes.INCTEN,
    value: val,
  }
}

export { increment, 
  decrement, 
  incrementByTen } from './Counter';

export { storeResult } from './Result';
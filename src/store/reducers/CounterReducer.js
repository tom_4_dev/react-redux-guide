import * as actionTypes from './../actions/ActionConstant';
import { updateObject } from './../utility';

const initialState = {
  counter: 0,
}

const counter = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.INC:
      return updateObject(state, { counter: state.counter + 1 })
    case actionTypes.DEC:
      return updateObject(state, { counter: state.counter - 1 })
    case actionTypes.INCTEN:
      return updateObject(state, { counter: state.counter + action.value })
    default:
      return state;
  }
}

export default counter;